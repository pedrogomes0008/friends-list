import React from 'react';
import Person from './Person';

function PersonList() {
	const peoples = [
		{
			img: 65,
			name: 'John',
			job: 'developer'
		},
		{
			img: 44,
			name: 'Bob',
			job: 'analist'
		},
		{
			img: 55,
			name: 'Peter',
			job: 'designer'
		}
	];
	return (
		<section>
			<Person person={peoples[0]} />
			<Person person={peoples[1]}>
				{' '}
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex quasi dolore incidunt recusandae odit.
			</Person>
			<Person person={peoples[2]} />
		</section>
	);
}

export default PersonList;
