# Friends-LIST

This project is a friends-list made whit React. It's a simple project to pass properties to the different components.

## Available Scripts

In the project directory, you can run:

### `initialize the app`

First things first. To use this app, download as a zip file and then open in your IDE. This app was made using Visual Studio Code. Besides IDE, you need node.js in your pc as well npm or yarn.

https://code.visualstudio.com/ -> Link for IDE vsCode

https://nodejs.org/en/         -> Link for node (v14.17.1)

### `npm i`

To install all the dependecies just put this command on the terminal in the root path of the downloaded file and will create the node_modules.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

# Final Result

![Alt-Text](src/images/PersonList.PNG)


